.PHONY: default test all
default: copy 

.PHONY: copy
copy:
	cp ../cv/*.html ./
	cp ../cv/style.css ./
	git commit -a -m "update html"
